package com.example.javafx.controller;

import com.example.javafx.InfoFromMediaFile;
import com.example.javafx.network.DownloaderFile;
import com.example.javafx.network.Response;
import com.example.javafx.utils.DataFromInputFile;
import com.example.javafx.utils.InfoFromUrl;
import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.UnsupportedTagException;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class PlayerController {
    @FXML
    public Label nameMusic;
    @FXML
    private Slider sliderMusic;
    @FXML
    private ImageView imageMusic;
    @FXML
    private Label beginSec;
    @FXML
    private Label endSec;
    @FXML
    private ImageView buttonImage;

    private MediaPlayer mediaPlayer;

    private static final String pathPause = "./src/main/resources/images/pause.png";
    private static final String pathPlay = "./src/main/resources/images/play.png";

    @FXML
    protected void onPlayButtonClick() {
        if (mediaPlayer != null) {
            if (mediaPlayer.getStatus() == MediaPlayer.Status.PLAYING) {
                mediaPlayer.pause();
                setImage(new File(pathPlay));
            } else {
                mediaPlayer.play();
                setImage(new File(pathPause));
            }
        }
    }

    private void updatesValues() {
        Platform.runLater(() -> sliderMusic.setValue(mediaPlayer.getCurrentTime().toMillis() /
                mediaPlayer.getTotalDuration().toMillis() * 100));

        beginSec.setText(millisecondsToTime((int) mediaPlayer.getCurrentTime().toMillis()));
    }

    private void downloadImage(String urlPicture, String pathPicture) {

        DownloaderFile downloader = new DownloaderFile();
        downloader.liveData.observe(this::setImageMusic);
        downloader.download(urlPicture, pathPicture);
    }

    private void downloadMusic(String urlMusic, String pathMusic) {

        DownloaderFile downloader = new DownloaderFile();
        downloader.liveData.observe(this::playMusic);
        downloader.download(urlMusic, pathMusic);
    }

    private void playMusic(Response<File> musicFile) {

        if (musicFile.isError()) {
            System.out.println
                    (musicFile.getError());
            return;
        }
        sliderMusic.setDisable(false);
        Media sound = new Media(musicFile.getSuccess().toURI().toString());
        mediaPlayer = new MediaPlayer(sound);

        mediaPlayer.setOnReady(() -> {
            endSec.setText(millisecondsToTime((int) sound.getDuration().toMillis()));
            setInfoMusic(musicFile.getSuccess());
            setImage(new File(pathPause));
            mediaPlayer.play();
        });

        mediaPlayer.currentTimeProperty().addListener(ov -> updatesValues());
        sliderMusic.valueProperty().addListener(ov -> {
            if (sliderMusic.isPressed()) {
                mediaPlayer.seek(mediaPlayer.getMedia().getDuration().multiply(sliderMusic.getValue() / 100));
            }
        });
    }

    private void setImageMusic(Response<File> imageMusicFile) {
        if (imageMusicFile.isError()) {
            System.out.println(imageMusicFile.getError());
            return;
        }
        Image image = new Image(imageMusicFile.getSuccess().toURI().toString(), 300, 300, false, false);
        imageMusic.setImage(image);
    }

    private void setImage(@NotNull File musicFile) {
        Image image = new Image(musicFile.toURI().toString(), 30, 30, false, false);
        buttonImage.setImage(image);
    }

    private String millisecondsToTime(int millis) {
        int min = (int) TimeUnit.MILLISECONDS.toMinutes(millis);
        int sec = (int) (TimeUnit.MILLISECONDS.toSeconds(millis) % 60);
        return String.format("%02d:%02d", min, sec);
    }

    private void setInfoMusic(File music) {
        try {
            InfoFromMediaFile info = new InfoFromMediaFile(new Mp3File(music.getPath()));
            nameMusic.setText(info.getAuthorName() + " - " + info.getSongName());
        } catch (IOException | UnsupportedTagException | InvalidDataException e) {
            e.printStackTrace();
        }
    }

    public void downloadFiles() {

        FileChooser fileChooser = new FileChooser();
        File fileFromDownload = fileChooser.showOpenDialog(new Stage());

        if (fileFromDownload != null) {
            if (fileFromDownload.isFile()) {
                InfoFromUrl infoFromUrl = new InfoFromUrl();
                File inputFile = new File(fileFromDownload.getPath());
                DataFromInputFile inputFileTransformation = new DataFromInputFile(inputFile);

                String urlImage = inputFileTransformation.getImageUrlString();
                String pathImage = inputFileTransformation.getImagePathString() + "\\" + infoFromUrl.getNameOfFile(urlImage) + "." + infoFromUrl.getTypeOfFile(urlImage);
                downloadImage(urlImage, pathImage);

                String urlMusic = inputFileTransformation.getMusicUrlString();
                String pathMusic = inputFileTransformation.getMusicPathString() + "\\" + infoFromUrl.getNameOfFile(urlMusic) + "." + infoFromUrl.getTypeOfFile(urlMusic);
                downloadMusic(urlMusic, pathMusic);

            } else {
                setError("Файл не выбран");
            }
        } else {
            setError("Файл не выбран");
        }
    }

    private void setError(String messageError) {

        Alert alert = new Alert(Alert.AlertType.ERROR);

        alert.setTitle("Ошибка");
        alert.setHeaderText("Ой, что-то произошло");
        alert.setContentText(messageError);

        alert.showAndWait();
    }
}