package com.example.javafx.network;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;

public interface Downloader {
    void download(String url, String destPath) throws IOException, URISyntaxException, ExecutionException, InterruptedException;
}
