package com.example.javafx.observer;

public interface Observer<T> {
    void onChange(T type);
}
