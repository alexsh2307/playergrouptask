package com.example.javafx.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class DataFromInputFile {

    private File file;
    private String musicUrlString;
    private String musicPathString;
    private String imageUrlString;
    private String imagePathString;

    public DataFromInputFile(File file) {
        this.file = file;
        fileReader();
    }

    private void fileReader() {
        try {
            List<String> list = Files.readAllLines(file.toPath());
            transformInputString(list.get(1), list.get(0));
        } catch (FileNotFoundException e) {
            System.out.println("Файл не найден");
        } catch (IOException e) {
            System.out.println("Ошибка считывания");
        }

    }

    private void transformInputString(String imageInputString, String musicInputString) {
        String[] imageArray = imageInputString.split(" ");
        String[] musicArray = musicInputString.split(" ");
        if (imageArray.length == 2 && musicArray.length == 2) {
            musicUrlString = musicArray[0];
            musicPathString = musicArray[1];
            imageUrlString = imageArray[0];
            imagePathString = imageArray[1];
        } else {
            System.out.println("В файле заданы некорректные значения");
        }
    }

    public String getMusicUrlString() {
        return musicUrlString;
    }

    public String getMusicPathString() {
        return musicPathString;
    }

    public String getImageUrlString() {
        return imageUrlString;
    }

    public String getImagePathString() {
        return imagePathString;
    }


}
