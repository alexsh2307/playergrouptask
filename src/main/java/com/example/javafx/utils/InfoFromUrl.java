package com.example.javafx.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InfoFromUrl {
    private Pattern typePattern = Pattern.compile("\\.(.*?)(\\?|&|$)");
    private Pattern namePattern = Pattern.compile("(.*?)\\.");

    public String getNameOfFile(String url) {
        url = url.substring(url.lastIndexOf("/")).replaceAll("/", "");
        Matcher matcher = namePattern.matcher(url);
        if (matcher.find()) {
            return matcher.group(1);
        } else {
            throw new IllegalArgumentException();
        }
    }

    public String getTypeOfFile(String url) {
        url = url.substring(url.lastIndexOf("/")).replaceAll("/", "");
        Matcher matcher = typePattern.matcher(url);
        if (matcher.find()) {
            return matcher.group(1);
        } else {
            throw new IllegalArgumentException();
        }
    }

}
